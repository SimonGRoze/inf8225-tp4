from bidict import bidict
from communities import Communities

import math
import community as community_louvain
import copy
import networkx as nx
from community import community_louvain
import operator
import threading


class Dynamo_Algorithm:

    def __init__(self, graph_old, communities_old):
        self.network_old = graph_old
        self.network_new = graph_old
        self.communities_old = communities_old
        self.communities_new = communities_old

    def addNewData(self, modifiedEdges, addedNodes):

        # "new" snapshot is now past
        self.network_old = copy.deepcopy(self.network_new)

        self.network_new.add_nodes_from(addedNodes)
        self.network_new.add_edges_from(modifiedEdges)
        # print(self.network_new.number_of_edges())
        # print("New version of the network's edges: ",self.network_new.edges())

        newCommunities = self.dynamoAlgorithm(modifiedEdges, addedNodes)

        return newCommunities

    def dynamoAlgorithm(self, modifiedEdges, modifiedNodes):
        # Applies the Dynamo algorithm.
        #
        # Input: graph_old, graph_new, communities_old
        # Returns: communities_new

        c1, c2 = self.dynamoInit(modifiedEdges, modifiedNodes)
        # print("Communities to be initialized as Singletons:",c1)
        # print("Nodes to be initialized as Two-Vertices:",c2)

        self.communities_new = copy.deepcopy(self.communities_old)
        for single_community in c1:
            # get all nodes in community
            nodes = self.communities_new.nodeMap.inverse[single_community]
            for node in list(nodes):
                # initialize singleton communities
                self.communities_new.nodeMap[node] = self.communities_new.nextId
                self.communities_new.nextId += 1
        for duo_nodes in c2:
            # initialize two-vertices community for tuples
            for node in duo_nodes:
                self.communities_new.nodeMap[node] = self.communities_new.nextId
            self.communities_new.nextId += 1

        for node in modifiedNodes:
            if node not in self.communities_new.nodeMap:
                # New node has no edges, add its own community
                self.communities_new.nodeMap[node] = self.communities_new.nextId
                # Add null edge
                self.network_new.add_edge(node, node, weight=0)
                self.communities_new.nextId += 1

        # print("communities initialization complete")

        bigGraph = self.makeCommunityGraph()

        # print("running the louvain algorithm")
        # Run the louvain algorithm
        partition = community_louvain.best_partition(bigGraph)

        # print("assigning new communities")
        for node in self.network_new.nodes():
            prev_comm = self.communities_new.nodeMap[node]
            self.communities_new.nodeMap[node] = partition[prev_comm]

        self.communities_old = copy.deepcopy(self.communities_new)

        return self.communities_new.nodeMap

    def dynamoInit(self, modifiedEdges, addedNodes):
        c1 = set()
        c2 = set()
        for addEdge in modifiedEdges:
            node_1 = addEdge[0]
            node_2 = addEdge[1]
            for k in addEdge:

                # Case 3: Node(Vertex) Addition
                if k in addedNodes:
                    wmax = 0
                    comm_2 = None
                    for edge in self.network_new.edges(k):
                        # WE NEED TO MAKE SURE THAT EACH ADDED NODE HAS AT LEAST ONE EDGE
                        weight = self.network_new.get_edge_data(edge[0], edge[1])["weight"]
                        if weight > wmax:
                            wmax = weight
                            comm_2 = (k, edge[1])
                            # TODO: if edge[1] is another newly added vertice
                            c1.add(self.communities_old.nodeMap[edge[1]])
                    # The link with the most weight is initialized as a two-vertices community
                    c2.add(comm_2)
                    # check w value for each community this
                    # vertice is linked to

            if node_1 not in addedNodes and node_2 not in addedNodes:
                if self.network_old.has_edge(node_1, node_2) and ((not self.network_new.has_edge(node_1, node_2)) or (
                        self.network_old.get_edge_data(node_1, node_2)["weight"] >
                        self.network_new.get_edge_data(node_1, node_2)["weight"])):
                    if self.communities_old.nodeMap[node_1] == self.communities_old.nodeMap[node_2]:
                        # Intra community weight increase. Initialize this community and all communities adjacent to
                        # node_1 and node_2 as singleton communities
                        c1.add(self.communities_old.nodeMap[node_1])
                        for k in addEdge:
                            for edge in self.network_old.edges(k):
                                c1.add(self.communities_old.nodeMap[edge[1]])


                elif (not self.network_old.has_edge(node_1, node_2)) or (
                        self.network_old.get_edge_data(node_1, node_2)["weight"] <
                        self.network_new.get_edge_data(node_1, node_2)["weight"]):
                    if self.communities_old.nodeMap[node_1] == self.communities_old.nodeMap[node_2]:
                        # Intra community weight increase. 2 cases are possible: keeping the community structure unchanged,
                        # or dividing the community into smaller communities. To acount for theses cases, initialize the two nodes
                        # as a two-vertice community and the other nodes in the community as singleton communities.
                        comm_2 = (node_1, node_2)
                        # two-vertices community
                        c2.add(comm_2)
                        # community of node_1 and node_2 marked as singleton communities
                        c1.add(self.communities_old.nodeMap[node_1])
                    else:
                        # Cross community weight increase. If the weight increase is greater than the threshold,
                        # make the same approach as the intra-community weight increase, else keep the structure unchanged.
                        if (not self.network_old.has_edge(node_1, node_2)):
                            delta_w = self.network_new.get_edge_data(node_1, node_2)["weight"]
                        else:
                            delta_w = self.network_new.get_edge_data(node_1, node_2)["weight"] - \
                                      self.network_old.get_edge_data(node_1, node_2)["weight"]
                        if self.compute_threshold(node_1, node_2, delta_w):
                            comm_2 = (node_1, node_2)
                            # two-vertices community
                            c2.add(comm_2)
                            # community of node_1 and node_2 marked as singleton communities
                            c1.add(self.communities_old.nodeMap[node_1])
                            c1.add(self.communities_old.nodeMap[node_2])
        return c1, c2

    def compute_threshold(self, node_i, node_j, delta_w):
        """

        Computes the threshold to decide whether to merge communities
        or divide them with a edge increase between two nodes of
        different communities.

        Parameters:

        - node_i and node_j: the two nodes with edge increase
        - delta_w: the delta of the weight with the last epoch

        """
        comm_i_nodes = self.communities_new.nodeMap.inverse[self.communities_new.nodeMap[node_i]]
        comm_j_nodes = self.communities_new.nodeMap.inverse[self.communities_new.nodeMap[node_j]]

        comm_i_j_edges = nx.edge_boundary(self.network_new, comm_i_nodes, comm_j_nodes, data='weight')
        # total weight of intra edges in both communities
        alpha2 = sum([x[2] for x in comm_i_j_edges])  # - community_k edges intra

        node_i_degree = self.network_new.degree(node_i, 'weight')
        node_j_degree = self.network_new.degree(node_j, 'weight')
        # beta2 is the sum of the degrees of the nodes
        beta2 = node_i_degree + node_j_degree

        m = self.network_new.number_of_edges()

        d1 = 2 * m - alpha2 - beta2
        d2 = (m * alpha2) + (node_i_degree * node_j_degree)
        return 2 * delta_w + d1 > math.sqrt(pow(d1, 2) + (4 * d2))

    def makeCommunityGraph(self):
        # To make updating the model easier, we treat each communities as a singular node. This method constructs a graph
        # with all the communities as nodes so that we can apply the louvain algorithm and save time.
        bigGraph = nx.Graph()

        for node1, node2, weight in self.network_new.edges(data='weight'):
            comm_node1 = self.communities_new.nodeMap[node1]
            comm_node2 = self.communities_new.nodeMap[node2]
            prev_weight = bigGraph.get_edge_data(comm_node1, comm_node2, {weight: 0}).get(weight, 1)
            bigGraph.add_edge(comm_node1, comm_node2, weight=weight + prev_weight)

        return bigGraph

    def getNeighborCommunity(self, communityId):
        # If there are singleton communities after the algorithm, find the adjacent community for suggestions
        wmax = 0
        boundary_edges = nx.edge_boundary(self.network_new, self.communities_new.nodeMap.inverse[communityId],
                                          data='weight')
        for edge in boundary_edges:
            if edge[2] > wmax:
                wmax = edge[2]
                comm_2 = self.communities_new.nodeMap[edge[1]]
        return comm_2
